import React from 'react';
import './OrderDetailsItem.css';

const OrderDetailsItem = props => (
    <div className="OrderDetailsItem">
        <h6>{props.name}</h6>
        <span>x{props.count}</span>
        <span>{props.price} KGS</span>
        <button onClick={props.onClick} className="Delete">x</button>
    </div>
);

export default OrderDetailsItem;
