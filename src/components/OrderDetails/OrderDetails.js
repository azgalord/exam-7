import React from 'react';
import './OrderDetails.css';

const OrderDetails = props => (
    <div className="OrderDetails">
        <h2>Order Details:</h2>
        <p className={props.messageEmpty}>
            Order is empty!
            <br/>
            Please add some items!
        </p>
        <p className={props.messageTotal}>
            {props.children}
            Total price: {props.totalPrice} KGS
        </p>
    </div>
);

export default OrderDetails;
