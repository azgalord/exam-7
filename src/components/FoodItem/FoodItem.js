import React from 'react';
import './FoodItem.css';

const FoodItem = props => (
    <div className="FoodItem" onClick={props.onClick}>
        <img src={props.src} alt=""/>
        <div className="FoodInfo">
            <h4>{props.name}</h4>
            <p>Price: {props.price} KGS</p>
        </div>
    </div>
);

export default FoodItem;
