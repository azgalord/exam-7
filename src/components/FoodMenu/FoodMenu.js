import React from 'react';
import './FoodMenu.css';

const FoodMenu = props => (
    <div className="FoodMenu">
        <h2>Add items:</h2>
        {props.children}
    </div>
);

export default FoodMenu;
