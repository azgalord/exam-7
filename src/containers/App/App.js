import React, {Component} from 'react';
import OrderDetails from '../../components/OrderDetails/OrderDetails';
import FoodMenu from '../../components/FoodMenu/FoodMenu';
import FoodItem from '../../components/FoodItem/FoodItem';
import OrderDetailsItem from "../../components/OrderDetailsItem/OrderDetailsItem";

import Hamburger from '../../assets/burger.jpg';
import HotDog from '../../assets/hot-dog-512.png';
import Coffee from '../../assets/coffee.jpg';
import Tea from '../../assets/TeaCup.png';
import Fries from '../../assets/french-fries-icon-flat-style-vector-19368521.jpg';
import Cola from '../../assets/cola.jpg';

import './App.css';

class App extends Component {
    state = {
        foodItems: [
            {name: 'Hamburger', price: 80, icon: Hamburger, count: 0},
            {name: 'Hot-dog', price: 60, icon: HotDog, count: 0},
            {name: 'Coffee', price: 70, icon: Coffee, count: 0},
            {name: 'Tea', price: 50, icon: Tea, count: 0},
            {name: 'Fries', price: 45, icon: Fries, count: 0},
            {name: 'Cola', price: 45, icon: Cola, count: 0}
        ],
        counts: 0
    };

    addItemToTotal = (event, index) => {
        event.preventDefault();
        const foodItems = [...this.state.foodItems];
        let counts = this.state.counts;
        foodItems[index].count++;
        counts++;

        this.setState({foodItems, counts});
    };

    deleteElement = (event, index) => {
        event.preventDefault();
        const foodItems = [...this.state.foodItems];
        foodItems[index].count--;
        this.state.counts--;

        this.setState({foodItems});
    };

    getOrderDetails = () => {
        const items = [];

        this.state.foodItems.map((object, index) => {
            if (object.count !== 0) {
                items.push(
                    <OrderDetailsItem
                        name={object.name}
                        price={object.price}
                        count={object.count}
                        onClick={(event) => (this.deleteElement(event, index))}
                    />
                )
            }
        });

        return items;
    };

    getTotalPrice = () => {
        let total = 0;
        this.state.foodItems.map((object) => {
            total += (object.price * object.count);
        });
        return total;
    };

    render() {
        return (
            <div className="App">
                <OrderDetails
                    messageEmpty={(this.state.counts > 0) ? 'Hidden' : 'Visible'}
                    messageTotal={(this.state.counts > 0) ? 'Visible' : 'Hidden'}
                    totalPrice={this.getTotalPrice()}
                >
                    {this.getOrderDetails()}
                </OrderDetails>
                <FoodMenu items={this.state.foodItems}>
                    {this.state.foodItems.map((object, index) => (
                        <FoodItem
                            onClick={(event) => (this.addItemToTotal(event, index))}
                            src={object.icon} name={object.name}
                            price={object.price} key={index}/>
                    ))}
                </FoodMenu>
            </div>
        );
    }
}

export default App;
